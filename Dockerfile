FROM node:alpine

RUN npm config set registry https://registry.npm.taobao.org/
RUN npm install -g eslint

CMD ["eslint"]
